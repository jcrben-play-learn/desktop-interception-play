notes on using interception tools:

run to get the data:
uinput -p -d /dev/input/by-id/my-kbd

TODO: write a simple daemon, read from it using the journal

https://stackoverflow.com/questions/17954432/creating-a-daemon-in-linux

http://0pointer.de/blog/projects/journal-submit.html

https://superuser.com/questions/162152/disabling-keys-from-a-t60-thinkpad-laptop | keyboard - Disabling keys from a T60 thinkpad laptop - Super User
https://wiki.archlinux.org/index.php/Extra_keyboard_keys | Extra keyboard keys - ArchWiki
https://stackoverflow.com/questions/15880196/keyboard-device-in-unix | linux - Keyboard device in Unix - Stack Overflow
https://stackoverflow.com/questions/11573974/write-to-txt-file | c - Write to .txt file? - Stack Overflow
https://github.com/NixOS/nixpkgs/blob/master/nixos/modules/services/hardware/interception-tools.nix | nixpkgs/interception-tools.nix at master · NixOS/nixpkgs
https://github.com/NixOS/nixpkgs/blob/master/pkgs/tools/inputmethods/interception-tools/default.nix | nixpkgs/default.nix at master · NixOS/nixpkgs
https://support.lenovo.com/us/en/solutions/ht002061 | How to use the function (Fn) keys on the keyboard - ThinkPad - US
https://superuser.com/questions/1206887/how-can-i-fix-the-fn-key-default-behaviour | ubuntu - How can I "fix" the "Fn" key default behaviour? - Super User
https://www.reddit.com/r/thinkpad/comments/75m3h4/disable_page_up_page_down_keys_on_thinkpads/ | Disable page up/ page down keys on Thinkpads : thinkpad
https://askubuntu.com/questions/1051769/how-to-disable-or-delay-pgdn-and-pgup-buttons-on-lenovo-thinkpad | keyboard - How to disable or delay pgdn and pgup buttons on Lenovo Thinkpad - Ask Ubuntu
https://gitlab.com/interception/linux/tools#installation | interception / linux / Interception Tools · GitLab
https://askubuntu.com/questions/979359/how-do-i-install-caps2esc | shortcut keys - How do I install caps2esc? - Ask Ubuntu
